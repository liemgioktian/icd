$(function () {
  var wlh = window.location.href
  var wlhx = wlh.split('?')
  var mode = null
  if (wlhx[1]) mode = wlhx[1].split('&')[0].replace('mode=','')
  wlh = wlhx[0]
	var activemenu = $('.subnavbar ul li a[href="'+wlh+'"]')
	if (activemenu.length > 0) activemenu.parent('li').addClass('active')
	else $('.subnavbar ul li a').eq(0).parent('li').addClass('active')

  $('.subnavbar ul li.active').each (function () {
	  var title = $(this).find('a span').html()
	  $('.widget-header h3').html(title)
	  $('.tabbable ul.nav.nav-tabs li a').each (function () {
	    var ori = $(this).text()
	    $(this).text(ori + ' ' + title)
	  })
	})

  if (null !== mode && mode == 'edit') {
    $('.tab-pane').removeClass('active').last().addClass('active')
    $('ul.nav.nav-tabs li').removeClass('active').last().addClass('active').find('a').each (function () {
      var txt = $(this).text()
      $(this).text($(this).text().replace('Input', 'Edit'))
    })
  }

  $('.alert').removeClass('hidden').hide()
  $('a[href*="?mode=delete&id="]').each(function () {
    var href = $(this).attr('href')
    $(this).unbind('click').bind('click', function (e) {
      $('.alert')
        .slideDown()
        .find('.btn').each (function () {
          $(this).unbind('click').bind('click', function () {
            if ($(this).is('.ya')) window.location = href
            else if ($(this).is('.tidak')) $('.alert').slideUp()
          })
        })
      e.preventDefault()
    })
  })

  var datefields = ['tanggalLahir', 'tanggal']
  for (var df in datefields) 
    if ($('input[name="'+datefields[df]+'"]').length > 0) 
      $('input[name="'+datefields[df]+'"]').datepicker({
        format: 'yyyy-mm-dd'
      })

  $('select').not('[name="diagnosa"], [name="code"]').select2()
  initDDdiagnosa(), initDDcode()

  function initDDdiagnosa () {
    $('[name="diagnosa"]').select2({
      ajax: {
        url: site_url + '/proses/autocomplete',
        type: 'GET', dataType: 'json',
        processResults: function (data) {
          return {results: data}
        }, cache: true
      }
    })
  }

  function initDDcode () {
    if ($('[name="code"]').find('option').length > 0) {
      var option = $('[name="code"]').find('option').html()
      option = option.split('-')
      option = option[0]
      $('[name="code"]').find('option').html(option)
    }
    $('[name="code"]').select2({
      ajax: {
        url: site_url + '/proses/autocomplete/codeonly',
        type: 'GET', dataType: 'json',
        processResults: function (data) {
          return {results: data}
        }, cache: true
      }
    })    
  }

  $('[name="diagnosa"]').change(function () {
    var id = $(this).val()
    var timestamp = new Date().getTime()
    $.get(site_url + '/proses/query/diagnosa?id=' + id + '&t=' + timestamp, function (icd) {
      icd = JSON.parse(icd)
      $('[name="code"]').html('')
      $('[name="code"]').append('<option value="'+id+'">'+icd[0].code+'</option>').select2('destroy')
      initDDcode()
    })
  })

  $('[name="code"]').change(function () {
    var id = $(this).val()
    var timestamp = new Date().getTime()
    $.get(site_url + '/proses/query/diagnosa?id=' + id + '&t=' + timestamp, function (icd) {
      icd = JSON.parse(icd)
      $('[name="diagnosa"]').html('')
      var text = icd[0].code + ' - ' + icd[0].specification +' (' +icd[0].type+ ')'
      $('[name="diagnosa"]').append('<option value="'+id+'">'+text+'</option>').select2('destroy')
      initDDdiagnosa()
    })
  })

  $('[name="nip"],[name="pegawai"]').change(function () {
    var id = $(this).val()
    var timestamp = new Date().getTime()
    $.get(site_url + '/proses/query/pegawai?id=' + id + '&t=' + timestamp, function (pegawai) {
      pegawai = JSON.parse(pegawai)
      pegawai = pegawai[0]
      $('[name="nip"]').val(pegawai.id).select2('destroy').val(pegawai.id).select2()
      $('[name="golongan"]').val(pegawai.golongan)
      $('[name="pegawai"]').select2('destroy').val(pegawai.id).select2()
    })
    $.get(site_url + '/proses/query/pasien?pegawai=' + id + '&t=' + timestamp, function (pasiens) {
      pasiens = JSON.parse(pasiens)
      $('[name="pasien"]').select2('destroy').html('')
      for (var p in pasiens) $('[name="pasien"]').append(
          '<option value="'+pasiens[p].id+
          '" data-ttl="'+pasiens[p].tanggalLahir+
          '" data-status="'+pasiens[p].hubunganPegawai+
          '">'+pasiens[p].nama+'</option>'
      )
      $('[name="pasien"]').select2().trigger('change')
    })
  })

  $('[name="pasien"]').change(function () {
    var status = ''
    var self = $(this).find('option:selected')
    $('[name="tanggalLahir"]').val(self.attr('data-ttl'))
    hitungUsia()

    switch (self.attr('data-status')) {
      case '0': status = 'PEGAWAI'; break
      case '1': status = 'ISTRI'; break
      case 'T': status = 'SUAMI'; break
      default : status = 'ANAK'; break
    }
    $('[name="hubunganPegawai"]').val(status)
  })

  $('select.peringkat').change(function () { window.location = $(this).val() })

  function hitungUsia () {
    var ttl = $('[name="tanggalLahir"]')
    if (ttl.val().length < 1) return true
    var selisih = Date.now() - new Date(ttl.val())
    var umur = new Date(selisih)
    var tahun = umur.getUTCFullYear() - 1970
    var bulan = umur.getMonth()
    var ttlp = $('[name="tanggalLahir"]').parent()
    ttlp.find('p.help-block').remove()
    ttlp.append('<p class="help-block">*) Usia : ' + tahun + ' tahun ' + bulan + ' bulan'+'</span>')
  }
  hitungUsia()

});