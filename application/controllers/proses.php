<?php

Class proses extends my_controller {


  function __construct () {
    parent::__construct();
  }

  function index () {
    $model = 'kunjungan';
    $get = $this->input->get();
    $post = $this->input->post();
    $data = array('passparam' => array());
    $this->load->model($model);

    if ($get && $get['mode'] == 'delete' && $get['id']) {
      $this->$model->delete($get['id']);
      redirect (current_url());
    }

    if ($post && $get && $get['id']) {
      $this->$model->save($post, $get['id']);
      redirect (current_url());
    } else if ($post) {
      $data['passparam'] ['afterinsert']= true;
      $this->$model->save($post);
    }

    $data['passparam']['items'] = $this->$model->find();
    $data['passparam']['theads'] = $this->$model->theads();

    $fields = $this->$model->fields();
    if ($get && $get['mode'] == 'edit') {
      $record = $this->$model->findOne($get['id']);
      foreach ($fields as &$field) $field['value'] = $record[$field['name']];
      #echo '<pre>';print_r($fields);die();
      $fields = $this->$model->editMode($fields);
    } else foreach ($fields as &$field) $field['value'] = '';
    $data['passparam']['fields'] = $fields;

    $this->loadview($data);
  }

  function peringkat () {
    $triwulan = $this->input->get('triwulan');
    $tahun = $this->input->get('tahun');
    $triwulan = !$triwulan ? null : $triwulan;
    $tahun = !$tahun ? null : $tahun;
    $this->load->model('report');
    $data = array();
    $data['viewer'] = 'peringkat';
    $data['passparam'] = array();
    $triwulan = $this->report->translateTriwulan ($triwulan, $tahun);
    $data['passparam']['top10'] = $this->report->top10($triwulan);
    $data['passparam']['topPerGolongan'] = $this->report->topPerGolongan($triwulan);
    $data['passparam']['freqKunjunganPerGol'] = $this->report->freqKunjunganPerGol($triwulan);
    $data['passparam']['triwulan'] = $this->report->triwulan($triwulan);
    $this->loadview($data);
  }

  function pegawai () {
    $this->crud('pegawai');
  }

  function pasien () {
    $this->crud('pasien');
  }

  function icd () {
    $this->crud('icd');
  }

  function autocomplete ($field = 'complete') {
    $this->load->database();
    $q = $this->input->get('q');
    $diagnosa = $this->db
      ->select($field == 'complete' ? "id, CONCAT (code, ' - ', specification,' (',type, ')') as text" : 'id, code as text', false)
      ->like('code', $q)
      ->or_like('specification', $q)
      ->limit(10)
      ->get('diagnosa')
      ->result();
    die(json_encode($diagnosa));
    /*$return = new stdClass();
    $return->total_count = count($diagnosa);
    $return->incomplete_results = 'false';
    $return->items = $diagnosa;
    die(json_encode($return));*/
  }

  function query ($entity) {
    $this->load->database();
    $get = $this->input->get();
    if (isset ($get['t'])) unset ($get['t']);
    die(json_encode($this->db->get_where($entity, $get)->result()));
  }
}