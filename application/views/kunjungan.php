<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">          
            
            <div class="widget ">
              
            <div class="widget-header">
              <h3>Loading, Please Wait ... </h3>
            </div> <!-- /widget-header -->
          
          <div class="widget-content">
            
            
                <form class="form-horizontal form-kunjungan" method="post">
                  <fieldset>
                    
                    <?php foreach ($fields as $field): ?>
                      <?php if (isset($field['options'])): ?>
                      <div class="control-group">                     
                        <label class="control-label"><?= $field['label'] ?></label>
                        <div class="controls">
                          <select class="span6" name="<?= $field['name'] ?>">
                            <?php foreach ($field['options'] as $option): ?>
                              <option value="<?= $option['value'] ?>"
                              <?= $option['value'] == $field['value'] ? 'selected':'' ?>
                              ><?= $option['text'] ?></option>
                            <?php endforeach ?>
                          </select>
                        </div> <!-- /controls -->       
                      </div> <!-- /control-group -->
                      <?php else : ?>
                      <div class="control-group">                     
                        <label class="control-label"><?= $field['label'] ?></label>
                        <div class="controls">
                          <input type="text" class="span6" name="<?= $field['name'] ?>" value="<?= $field['value'] ?>">
                        </div> <!-- /controls -->       
                      </div> <!-- /control-group -->
                      <?php endif ?>
                    <?php endforeach ?>

                      <!-- <div class="control-group">                     
                        <label class="control-label"></label>
                        <div class="controls">
                          <span style="color: red"> (*) hanya untuk pasien baru, kosongkan bila pasien telah terdaftar sebelumnya</span>
                        </div>
                      </div> -->                    
                     <br />
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary">Save</button> 
                      <a href="<?= current_url() ?>" class="btn btn-warning">Cancel</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
            
          </div> <!-- /widget-content -->
            
        </div> <!-- /widget -->
            
        </div> <!-- /span8 -->
          
          
          
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->