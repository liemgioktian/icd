<div class="main">
  <div class="main-inner">
      <div class="container">
        <div class="row">
          <div class="span12">          
            <div class="widget ">
            <div class="widget-header">
              <h3>Loading, Please Wait ... </h3>
              <select class="peringkat">
                <?php foreach ($triwulan as $opt) : ?>
                <option value="<?= $opt['value'] ?>" <?= $opt['selected']?'selected':'' ?>><?= $opt['text'] ?></option>
                <?php endforeach ?>
              </select>
            </div> <!-- /widget-header -->
          <div class="widget-content">

            <div class="row">
              <div class="span4">
                <table class="table table-bordered table-report">
                  <thead>
                    <tr>
                      <td colspan="2"><b>PERINGKAT PENYAKIT PEGAWAI</b></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $rank = 0; foreach ($top10 as $top): $rank++ ?>
                    <tr>
                      <td width="5%"><?= $rank ?></td>
                      <td><?= "$top->specification ($top->qty orang)" ?></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <div class="span4">
                <table class="table table-bordered table-report">
                  <thead>
                    <tr>
                      <td colspan="2"><b>PENYAKIT TERBANYAK PER GOLONGAN PEGAWAI</b></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($topPerGolongan as $top): ?>
                    <tr>
                      <td width="5%"><?= $top->golongan ?></td>
                      <td><?= $top->specification ?></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <div class="span3">
                <table class="table table-bordered table-report">
                  <thead>
                    <tr>
                      <td colspan="2"><b>FREKUENSI KUNJUNGAN PEGAWAI PER GOLONGAN</b></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($freqKunjunganPerGol as $top): ?>
                    <tr>
                      <td width="5%"><?= $top->golongan ?></td>
                      <td><?= $top->frekuensi ?> kunjungan</td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>

          </div> <!-- /widget-content -->
        </div> <!-- /widget -->
        </div> <!-- /span8 -->
          
        </div> <!-- /row -->
      </div> <!-- /container -->
  </div> <!-- /main-inner -->
</div> <!-- /main -->