<div class="subnavbar">

  <div class="subnavbar-inner">
  
    <div class="container">

      <ul class="mainnav">


        <?php foreach ($menus as $menu): ?>
        <li>
          <a href="<?= $menu[0] ?>">
            <i class="icon-<?=  $menu[1]?>"></i>
            <span><?= $menu[2] ?></span>
          </a>              
        </li>
        <?php endforeach; ?>


      </ul>

    </div> <!-- /container -->
  
  </div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->