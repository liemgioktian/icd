<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">          
            
            <div class="widget ">
              
            <div class="widget-header">
              <h3>Loading, Please Wait ... </h3>
            </div> <!-- /widget-header -->
          
          <div class="widget-content">
            
            
                  <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>NO</th>
                        <?php foreach ($theads as $th): ?>
                          <th><?= $th[1] ?></th>
                        <?php endforeach ?>
                        <th>ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no=0; foreach ($items as $item): $no++; ?>
                        <tr>
                          <td><?= $no ?></td>
                          <?php foreach ($theads as $th): $field = $th[0] ?>
                            <td><?= $item->$field ?></td>
                          <?php endforeach ?>
                          <td>
                            <a class="btn btn-small btn-warning" href="<?= current_url() . '?mode=delete&id=' . $item->id ?>">lihat riwayat penyakit</a>
                          </td>
                        </tr>
                      <?php endforeach ?>
                    </tbody>
                    <tfoot></tfoot>
                  </table>  
            

          </div> <!-- /widget-content -->
            
        </div> <!-- /widget -->
            
        </div> <!-- /span8 -->
          
          
          
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->