<?php

Class kunjungan extends my_model {

  var $table = 'kunjungan';
  var $theads = array (
    array('tanggal', 'TANGGAL'),
    array('pasien', 'PASIEN'),
    array('diagnosa', 'DIAGNOSA'),
  );
  var $fields = array (
    array (
      'label' => 'TANGGAL',
      'name' => 'tanggal'
    ),
    array (
      'label' => 'NIP',
      'name' => 'nip'
    ),
    array (
      'label' => 'NAMA PEGAWAI',
      'name' => 'pegawai'
    ),
    array (
      'label' => 'GOLONGAN',
      'name' => 'golongan'
    ),
    array (
      'label' => 'NAMA PASIEN',
      'name' => 'pasien'
    ),
    array (
      'label' => 'TANGGAL LAHIR',
      'name' => 'tanggalLahir'
    ),
    array (
      'label' => 'STATUS',
      'name' => 'hubunganPegawai'
    ),
    array (
      'label' => 'DIAGNOSA',
      'name' => 'diagnosa',
      'options' => array()
    ),
  );

  function __construct () {
    parent::__construct();
    /*
    $options = array(array('value'=>0, 'text'=>''));
    $this->db->select('pasien.id, pasien.nama, pegawai.noPegawai');
    $this->db->join('pegawai', 'pegawai.id = pasien.pegawai');
    $pasiens = $this->db->get('pasien')->result();
    foreach ($pasiens as $pasien) {
      $options [] = array (
        'value' => $pasien->id,
        'text' => "$pasien->noPegawai - $pasien->nama"
      );
    }
    $this->fields[1]['options'] = $options;
    */
    $nips = $namas = array(array('value'=>0, 'text'=>''));
    $pegawais = $this->db->get('pegawai')->result();
    foreach ($pegawais as $pegawai) {
      $nips [] = array (
        'value' => $pegawai->id,
        'text' => $pegawai->noPegawai
      );
      $namas [] = array (
        'value' => $pegawai->id,
        'text' => $pegawai->nama
      );
    }
    $this->fields[1]['options'] = $nips;
    $this->fields[2]['options'] = $namas;
    $this->fields[4]['options'] = array(array('value'=>0, 'text'=>''));
  }

  function find ($where = array()) {
    $this->db->select('kunjungan.*');
    $this->db->select("DATE_FORMAT(kunjungan.tanggal,'%d %M %Y') as tanggal", false);
    $this->db->select('pasien.nama as pasien', false);
    $this->db->select('CONCAT (diagnosa.code, " ", diagnosa.specification) as diagnosa', false);
    $this->db->join('pasien', 'kunjungan.pasien = pasien.id');
    $this->db->join('diagnosa', 'kunjungan.diagnosa = diagnosa.id');
    return $this->db->get($this->table)->result();
  }

  function findOne ($id) {
    $obj = $this->db->get_where($this->table, array('id' => $id))->row_array();
    $this->db->select("*, DATE_FORMAT(tanggalLahir,'%d %M %Y') as tanggalLahir", false);
    $pasien = $this->db->get_where('pasien', array('id' => $obj['pasien']))->row_array();
    $pegawai = $this->db->get_where('pegawai', array('id' => $pasien['pegawai']))->row_array();
    $obj['nip'] = $pegawai['id'];
    $obj['golongan'] = $pegawai['golongan'];
    $obj['pegawai'] = $pegawai['id'];
    $obj['tanggalLahir'] = $pasien['tanggalLahir'];
    $obj['hubunganPegawai'] = $pasien['hubunganPegawai'];
    return $obj;
  }

  function editMode ($fields) {
    $idPegawai = $fields[1]['value'];
    $idPasien = $fields[4]['value'];
    $pasien = $this->db->get_where('pasien', array('pegawai' => $idPegawai))->result();
    $fields[4]['options'] = array();
    foreach ($pasien as $p) $fields[4]['options'][] = array ('value' => $p->id, 'text' => $p->nama);

    $idIcd = $fields[7]['value'];
    $diagnosa = $this->db->get_where('diagnosa', array('id' => $idIcd))->row_array();
    $fields[7]['options'][0] = array ('value' => $diagnosa['id'], 'text' => $diagnosa['code'] . ' - ' . $diagnosa['specification']);

    switch ($fields[6]['value']) {
      case '0' : $fields[6]['value'] = 'PEGAWAI'; break;
      case '1' : $fields[6]['value'] = 'ISTRI'; break;
      default : $fields[6]['value'] = 'ANAK'; break;
    }
    return $fields;
  }

  function save ($post, $id = null) {
    $post = array (
      'tanggal' => $post['tanggal'],
      'pasien' => $post['pasien'],
      'diagnosa' => $post['diagnosa']
    );
    if (is_null($id)) $this->db->insert($this->table, $post);
    else $this->db->where('id', $id)->update($this->table, $post);
  }
}
