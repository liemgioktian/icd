<?php

Class report extends my_model {

  var $table = 'kunjungan';

  function top10 ($triwulan) {
  	$this->db->select('specification');
  	$this->db->select('count(distinct kunjungan.pasien) as qty', false);

  	$this->db->join('pasien', 'kunjungan.pasien = pasien.id and pasien.hubunganPegawai = 0');
  	$this->db->join('diagnosa', 'kunjungan.diagnosa = diagnosa.id');

    $this->db->where('kunjungan.tanggal >= ', $triwulan['startdate']);
    $this->db->where('kunjungan.tanggal <= ', $triwulan['enddate']);

  	$this->db->group_by('diagnosa.id');
  	$this->db->order_by('count(distinct kunjungan.pasien)', 'desc');
  	$this->db->limit(10);
  	return 
    $this->db->get('kunjungan')->result();
    // die($this->db->last_query());
  }

  function topPerGolongan ($triwulan) {
    $startdate = $triwulan['startdate'];
    $enddate = $triwulan['enddate'];
    $this->db->distinct();
    $this->db->select('golongan');
    $this->db->select("(SELECT 
      CONCAT(diagnosa.specification, ' (', COUNT(DISTINCT kunjungan.pasien), ' orang)')
      FROM kunjungan
      JOIN pasien ON kunjungan.pasien = pasien.id AND hubunganPegawai = 0
      JOIN pegawai ON pasien.pegawai = pegawai.id
      JOIN diagnosa ON kunjungan.diagnosa = diagnosa.id
      WHERE pegawai.golongan = parent.golongan
      AND kunjungan.tanggal >= '$startdate' AND kunjungan.tanggal <= '$enddate'
      GROUP BY diagnosa.id
      ORDER BY COUNT(DISTINCT kunjungan.pasien) desc
      LIMIT 1
    ) as specification", false);
    $this->db->where('golongan <>', '');
    $this->db->order_by('golongan');
    $this->db->from('pegawai parent', false);
    return 
    $this->db->get()->result();
    // die($this->db->last_query());
  }

  function freqKunjunganPerGol ($triwulan) {
    $this->db->select('golongan');
    $this->db->select('COUNT(kunjungan.id) as frekuensi', false);
    $this->db->join('pasien', 'kunjungan.pasien = pasien.id and pasien.hubunganPegawai = 0');
    $this->db->join('pegawai', 'pasien.pegawai = pegawai.id');
    $this->db->where('kunjungan.tanggal >= ', $triwulan['startdate']);
    $this->db->where('kunjungan.tanggal <= ', $triwulan['enddate']);
    $this->db->group_by('golongan');
    return 
    $this->db->get('kunjungan')->result();
    // die($this->db->last_query());
  }

  function translateTriwulan ($triwulan, $tahun) {
    $tahun = is_null ($tahun) ? date("Y") : $tahun;
    if (is_null ($triwulan))
      switch ((int) date('m')) {
        case 1: case 2:  case 3: $triwulan = 1; break;
        case 4: case 5:  case 6: $triwulan = 2; break;
        case 7: case 8:  case 9: $triwulan = 3; break;
        case 10: case 11:  case 12: $triwulan = 4; break;
      }
    $startdate = $enddate = '';
    switch ($triwulan) {
      case 1:
        $startdate = "$tahun-1-1";
        $enddate = "$tahun-3-31";
        break;
      case 2:
        $startdate = "$tahun-4-1";
        $enddate = "$tahun-6-30";
        break;
      case 3:
        $startdate = "$tahun-7-1";
        $enddate = "$tahun-9-30";
        break;
      case 4:
        $startdate = "$tahun-10-1";
        $enddate = "$tahun-12-31";
        break;
    }
    return array(
      'tahun' => $tahun,
      'triwulan' => $triwulan,
      'startdate' => $startdate,
      'enddate' => $enddate,
    );
  }

  function triwulan ($triwulan) {
    $dropdown = array();
    $minY = $this->db->query('select min(year(tanggal)) as tahun from kunjungan where year(tanggal) > 0')->row_array();
    $maxY = $this->db->query('select max(year(tanggal)) as tahun from kunjungan where year(tanggal) > 0')->row_array();
    for ($y = $minY['tahun']; $y <= $maxY['tahun']; $y++) {
      for ($tw = 1; $tw <= 4; $tw++) {
        $dropdown[] = array(
          'value' => site_url ("proses/peringkat?triwulan=$tw&tahun=$y"),
          'text' => "Triwulan ke-$tw Tahun $y",
          'selected' => $y == $triwulan['tahun'] && $tw == $triwulan['triwulan']
        );
      }
    }
    return $dropdown;
  }
}