<?php

Class pasien extends my_model {

  var $table = 'pasien';
  var $theads = array (
    array('noPegawai', 'NIP'),
    array('nama', 'NAMA LENGKAP'),
    array('tanggalLahir', 'TANGGAL LAHIR'),
    array('hubunganPegawai', 'STATUS'),
    array('pegawai', 'ATAS NAMA')
  );
  var $fields = array (
    array (
      'label' => 'NAMA PEGAWAI',
      'name' => 'pegawai'
    ),
    array (
      'label' => 'NAMA PASIEN',
      'name' => 'nama'
    ),
    array (
      'label' => 'TANGGAL LAHIR',
      'name' => 'tanggalLahir'
    ),
    array (
      'label' => 'JENIS KELAMIN',
      'name' => 'jenisKelamin'
    ),
    array (
      'label' => 'HUBUNGAN',
      'name' => 'hubunganPegawai'
    ),
  );

  function __construct () {
    parent::__construct();
    $options = array();
    foreach ($this->db->get('pegawai')->result() as $gol) {
      $option = array(
        'value' => $gol->id,
        'text' => $gol->nama
      );
      $options[] = $option;
    }
    $this->fields[0]['options'] = $options;
    $this->fields[3]['options'] = array (
      array (
        'value' => '1',
        'text' => 'PRIA'
      ),
      array (
        'value' => '0',
        'text' => 'WANITA'
      ),
    );
    $this->fields[4]['options'] = array (
      array (
        'value' => '0',
        'text' => 'PEGAWAI'
      ),
      array (
        'value' => '1',
        'text' => 'ISTRI'
      ),
      array (
        'value' => 'T',
        'text' => 'SUAMI'
      ),
      array (
        'value' => 'ABC',
        'text' => 'ANAK'
      ),
    );
  }

  function findOne ($id) {
    $found = parent::findOne($id);
    if (in_array($found['hubunganPegawai'], array('A','B','C'))) $found['hubunganPegawai'] = 'ABC';
    return $found;
  }

  function find ($where = array()) {
    $this->db->select('pasien.*');
    $this->db->select('pegawai.noPegawai');
    $this->db->select("DATE_FORMAT(pasien.tanggalLahir,'%d %M %Y') as tanggalLahir", false);
    $this->db->select('pegawai.nama as pegawai', false);
    $this->db->select("CASE hubunganPegawai ".
    "WHEN '0' THEN 'PEGAWAI' ".
    "WHEN '1' THEN 'ISTRI' ".
    "WHEN 'T' THEN 'SUAMI' ".
    "ELSE 'ANAK' END as hubunganPegawai", false);
    $this->db->join('pegawai', 'pegawai.id = pasien.pegawai');
    return $this->db->get($this->table)->result();
  }

}
