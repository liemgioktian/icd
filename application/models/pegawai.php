<?php

Class pegawai extends my_model {

  var $table = 'pegawai';
  var $theads = array (
    array('noPegawai', 'NOMOR PEGAWAI'),
    array('nama', 'NAMA LENGKAP'),
    array('tanggalLahir', 'TANGGAL LAHIR'),
    array('golongan', 'GOLONGAN'),
    array('pangkat', 'PANGKAT'),
    array('unitKerja', 'UNIT KERJA')
  );
  var $fields = array (
    array (
      'label' => 'NOMOR PEGAWAI',
      'name' => 'noPegawai'
    ),
    array (
      'label' => 'NAMA PEGAWAI',
      'name' => 'nama'
    ),
    array (
      'label' => 'JENIS KELAMIN',
      'name' => 'jenisKelamin'
    ),
    array (
      'label' => 'TANGGAL LAHIR',
      'name' => 'tanggalLahir'
    ),
    array (
      'label' => 'GOLONGAN',
      'name' => 'golongan'
    ),
  );

  function __construct () {
    parent::__construct();
    // $options = array();
    // foreach ($this->db->distinct()->select('golongan')->get('pegawai')->result() as $gol) {
      // $option = array(
        // 'value' => $gol->golongan,
        // 'text' => $gol->golongan
      // );
      // $options[] = $option;
    // }
    // $this->fields[4]['options'] = $options;
    $this->fields[2]['options'] = array (
      array (
        'value' => '1',
        'text' => 'PRIA'
      ),
      array (
        'value' => '0',
        'text' => 'WANITA'
      ),
    );
  }

}
