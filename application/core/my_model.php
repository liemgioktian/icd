<?php

Class my_model extends CI_Model {


  function __construct () {
    parent::__construct();
    $this->load->database();
  }

  function find ($where = array()) {
    return $this->db->get($this->table)->result();
  }

  function theads () {
    return isset($this->theads) ? $this->theads : array();
  }

  function fields () {
    return isset($this->fields) ? $this->fields : array();
  }

  function save ($post, $id = null) {
    if (is_null($id)) $this->db->insert($this->table, $post);
    else $this->db->where('id', $id)->update($this->table, $post);
  }

  function findOne ($id) {
    return $this->db->get_where($this->table, array('id' => $id))->row_array();
  }

  function delete ($id) {
    $this->db->where('id', $id)->delete($this->table);
  }

}
