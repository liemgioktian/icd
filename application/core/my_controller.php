<?php

Class my_controller extends CI_Controller {


  function __construct () {
    parent::__construct();
    $this->load->helper('url');
  }

  function loadview ($data) {

    $subnavbar['menus'] = array (
      array(site_url('proses/index'), 'calendar', 'Kunjungan Pasien'),
      array(site_url('proses/peringkat'), 'bar-chart', 'Peringkat Penyakit'),
      array(site_url('proses/pasien'), 'group', 'Pasien'),
      array(site_url('proses/pegawai'), 'user', 'Pegawai'),
      array(site_url('proses/icd'), 'stethoscope', 'Master ICD'),
    );

    $this->output->set_header("HTTP/1.0 200 OK");
    $this->output->set_header("HTTP/1.1 200 OK");
    $this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
    $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
    $this->output->set_header("Cache-Control: post-check=0, pre-check=0");
    $this->output->set_header("Pragma: no-cache");

    $this->load->view('template/highest');
    $this->load->view('template/navbar');
    $this->load->view('template/subnavbar', $subnavbar);

    if (isset($data['viewer'])) $this->load->view($data['viewer'], $data['passparam']);
    else $this->load->view('main', $data['passparam']);

    $this->load->view('template/extra');
    $this->load->view('template/footer');
    $this->load->view('template/lowest');
  }  

  function crud ($model) {
    $get = $this->input->get();
    $post = $this->input->post();
    $data = array('passparam' => array());
    $this->load->model($model);

    if ($get && $get['mode'] == 'delete' && $get['id']) {
      $this->$model->delete($get['id']);
      redirect (current_url());
    }

    if ($post && $get && $get['id']) {
      $this->$model->save($post, $get['id']);
      redirect (current_url());
    } else if ($post) $this->$model->save($post);

    $data['passparam']['items'] = $this->$model->find();
    $data['passparam']['theads'] = $this->$model->theads();

    $fields = $this->$model->fields();
    if ($get && $get['mode'] == 'edit') {
      $record = $this->$model->findOne($get['id']);
      foreach ($fields as &$field) $field['value'] = $record[$field['name']];
    } else foreach ($fields as &$field) $field['value'] = '';
    $data['passparam']['fields'] = $fields;

    $this->loadview($data);
  }
}